import React, { useState } from 'react'

export const ExampleComponent = () => {
  const [foo] = useState('example')
  return <div>Example Component: {foo}</div>
}
